<?php

namespace Modules\News\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\News\Entities\Category;
use Modules\News\Http\Requests\CreateCategoryRequest;
use Modules\News\Http\Requests\UpdateCategoryRequest;
use Modules\News\Repositories\CategoryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class CategoryController extends AdminBaseController
{
    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(CategoryRepository $category)
    {
        parent::__construct();

        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = $this->category->all();

        return view('news::admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data = [];

        $categorySelectOptions = [];
        $categorySelectOptions[0] = '-- Main Category --';
        $nestedCategories = $this->category->getNestedList('title', 'id', '&nbsp;&nbsp;&nbsp;');
        $categorySelectOptions = $categorySelectOptions + $nestedCategories;
        $data['categorySelectOptions'] = $categorySelectOptions;
        
        return view('news::admin.categories.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoryRequest $request
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $input = $request->all();
        if ($input['parent_id'] == 0) {
            unset($input['parent_id']);
        }
        $this->category->create($input);

        return redirect()->route('admin.news.category.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('news::categories.title.categories')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        $data = [];
        $data['category'] = $category;

        $categorySelectOptions = [];
        $categorySelectOptions[0] = '-- Main Category --';
        $nestedCategories = $this->category->getNestedList('title', 'id', '&nbsp;&nbsp;&nbsp;');
        // dd($nestedCategories);
        $categorySelectOptions = $categorySelectOptions + $nestedCategories;
        $data['categorySelectOptions'] = $categorySelectOptions;

        return view('news::admin.categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Category $category
     * @param  UpdateCategoryRequest $request
     * @return Response
     */
    public function update(Category $category, UpdateCategoryRequest $request)
    {
        $input = $request->all();
        if ($input['parent_id'] == 0) {
            unset($input['parent_id']);
            if ($category->getOriginal('parent_id') != 0) {
                $category->makeRoot();
            }
        }
        try {
            $this->category->update($category, $input);
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage());
        }

        return redirect()->route('admin.news.category.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('news::categories.title.categories')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return Response
     */
    public function destroy(Category $category)
    {
        $this->category->destroy($category);

        return redirect()->route('admin.news.category.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('news::categories.title.categories')]));
    }
}
