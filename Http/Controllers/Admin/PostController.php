<?php

namespace Modules\News\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\News\Entities\Post;
use Modules\News\Entities\Status;
use Modules\News\Http\Requests\CreatePostRequest;
use Modules\News\Http\Requests\UpdatePostRequest;
use Modules\News\Repositories\PostRepository;
use Modules\News\Repositories\CategoryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Media\Repositories\FileRepository;

class PostController extends AdminBaseController
{
     /**
     * @var PostRepository
     */
    private $post;
    /**
     * @var CategoryRepository
     */
    private $category;
    /**
     * @var FileRepository
     */
    private $file;
    /**
     * @var Status
     */
    private $status;

    public function __construct(PostRepository $post, CategoryRepository $category, FileRepository $file, Status $status)
    {
        parent::__construct();

        $this->post = $post;
        $this->category = $category;
        $this->file = $file;
        $this->status = $status;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = $this->post->all();

        return view('news::admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = $this->category->all();
        $statuses = $this->status->lists();
        // dd($categories);
        $this->assetPipeline->requireJs('ckeditor.js');
        
        return view('news::admin.posts.create', compact('categories', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePostRequest $request
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        // get rid null tag
        $request['tags'] = $request->input('tags', []);
        $post = $this->post->create($request->all());

        $categoryIds = $request->input('categories', []);
        if (!empty($categoryIds)) {
            $post->categories()->sync($categoryIds);
        }

        return redirect()->route('admin.news.post.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('news::posts.title.posts')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Post $post
     * @return Response
     */
    public function edit(Post $post)
    {
        $thumbnail = $this->file->findFileByZoneForEntity('thumbnail', $post);
        // dd($thumbnail);
        $categories = $this->category->all();
        $statuses = $this->status->lists();
        $this->assetPipeline->requireJs('ckeditor.js');

        return view('news::admin.posts.edit', compact('post', 'categories', 'thumbnail', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Post $post
     * @param  UpdatePostRequest $request
     * @return Response
     */
    public function update(Post $post, UpdatePostRequest $request)
    {
        // get rid null tag
        $request['tags'] = $request->input('tags', []);
        $this->post->update($post, $request->all());

        // Sync post with Categories
        $categoryIds = $request->input('categories', []);
        $post->categories()->sync($categoryIds);

        return redirect()->route('admin.news.post.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('news::posts.title.posts')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Post $post
     * @return Response
     */
    public function destroy(Post $post)
    {
        $this->post->destroy($post);

        return redirect()->route('admin.news.post.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('news::posts.title.posts')]));
    }
}
