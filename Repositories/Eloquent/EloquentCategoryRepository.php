<?php

namespace Modules\News\Repositories\Eloquent;

use Modules\News\Repositories\CategoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCategoryRepository extends EloquentBaseRepository implements CategoryRepository
{
    public function all()
    {
        $orderColumn = 'lft';
        $ordered = 'ASC';

        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations')->orderBy($orderColumn, $ordered)->get();
        }

        return $this->model->orderBy($orderColumn, $ordered)->get();
    }

    public function getNestedList($column, $key = null, $seperator = ' ')
    {
        return \Modules\News\Entities\Category::getNestedList($column, $key, $seperator);
    }
}
