<?php

namespace Modules\News\Repositories\Eloquent;

use Modules\News\Repositories\PostRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

use Modules\News\Events\PostIsCreating;
use Modules\News\Events\PostWasCreated;
use Modules\News\Events\PostIsUpdating;
use Modules\News\Events\PostWasUpdated;
use Modules\News\Events\PostWasDeleted;
use Modules\Blog\Repositories\Collection;

class EloquentPostRepository extends EloquentBaseRepository implements PostRepository
{
    /**
     * Create a post
     * @param  array $data
     * @return Post
     */
    public function create($data)
    {
        event($event = new PostIsCreating($data));
        $post = $this->model->create($event->getAttributes());

        $post->setTags(array_get($data, 'tags'));

        event(new PostWasCreated($post, $data));

        return $post;
    }

    /**
     * Update a resource
     * @param $post
     * @param  array $data
     * @return mixed
     */
    public function update($post, $data)
    {
        event($event = new PostIsUpdating($post, $data));
        $post->update($event->getAttributes());

        $post->setTags(array_get($data, 'tags'));

        event(new PostWasUpdated($post, $data));

        return $post;
    }

    public function destroy($model)
    {
        $model->untag();

        event(new PostWasDeleted($model->id, get_class($model)));

        return $model->delete();
    }

    /**
     * @param  int    $id
     * @return object
     */
    public function find($id)
    {
        return $this->model->with('translations', 'tags')->find($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->model->with('translations', 'tags')->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Return the latest x blog posts
     * @param int $amount
     * @return Collection
     */
    public function latest($amount = 5)
    {
        return $this->model->whereStatus(Status::PUBLISHED)->orderBy('created_at', 'desc')->take($amount)->get();
    }

    /**
     * Get the previous post of the given post
     * @param object $post
     * @return object
     */
    public function getPreviousOf($post)
    {
        return $this->model->where('created_at', '<', $post->created_at)
            ->whereStatus(Status::PUBLISHED)->orderBy('created_at', 'desc')->first();
    }

    /**
     * Get the next post of the given post
     * @param object $post
     * @return object
     */
    public function getNextOf($post)
    {
        return $this->model->where('created_at', '>', $post->created_at)
            ->whereStatus(Status::PUBLISHED)->first();
    }

    public function findBySlug($slug)
    {
        return $this->model->whereHas('translations', function (Builder $q) use ($slug) {
            $q->where('slug', "$slug");
        })->with('translations')->whereStatus(Status::PUBLISHED)->firstOrFail();
    }

}
