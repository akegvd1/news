<?php

namespace Modules\News\Repositories\Cache;

use Modules\News\Repositories\PostRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePostDecorator extends BaseCacheDecorator implements PostRepository
{
    public function __construct(PostRepository $post)
    {
        parent::__construct();
        $this->entityName = 'news.posts';
        $this->repository = $post;
    }
}
