<?php

namespace Modules\News\Entities;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'detail'];
    protected $table = 'news__category_translations';
}
