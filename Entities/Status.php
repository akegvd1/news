<?php

namespace Modules\News\Entities;

/**
 * Class Status
 * @package Modules\News\Entities
 */
class Status
{
    const DRAFT = 0;
    // const PENDING = 1;
    const PUBLISHED = 1;
    // const UNPUBLISHED = 3;

    /**
     * @var array
     */
    private $statuses = [];

    public function __construct()
    {
        $this->statuses = [
            self::DRAFT => "Draft",
            // self::PENDING => "pending",
            self::PUBLISHED => "Publish",
            // self::UNPUBLISHED => "unpublished",
        ];
    }

    /**
     * Get the available statuses
     * @return array
     */
    public function lists()
    {
        return $this->statuses;
    }

    /**
     * Get the post status
     * @param int $statusId
     * @return string
     */
    public function get($statusId)
    {
        if (isset($this->statuses[$statusId])) {
            return $this->statuses[$statusId];
        }

        return $this->statuses[self::DRAFT];
    }
}
