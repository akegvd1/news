<?php

namespace Modules\News\Entities;

// use Dimsav\Translatable\Translatable;
use Modules\News\Entities\Traits\CategoryTranslatable;

class Category extends \Baum\Node
{
    // use Translatable;
    use CategoryTranslatable;
    
    public $translationModel = 'Modules\News\Entities\CategoryTranslation';
    public $useTranslationFallback = true;

    protected $table = 'news__categories';
    public $translatedAttributes = ['title', 'detail'];
    protected $fillable = ['parent_id', 'slug'];
    
}