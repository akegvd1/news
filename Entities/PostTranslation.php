<?php

namespace Modules\News\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\News\Events\PostContentIsRendering;

class PostTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'slug', 'content'];
    protected $table = 'news__post_translations';

    public function getContentAttribute($content)
    {
        event($event = new PostContentIsRendering($content));

        return $event->getContent();
    }
}
