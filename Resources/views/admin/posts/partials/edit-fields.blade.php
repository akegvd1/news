<div class="box-body">
    <div class='form-group{{ $errors->has("$lang.title") ? ' has-error' : '' }}'>
        <?php $oldTitle = isset($post->translate($lang)->title) ? $post->translate($lang)->title : ''; ?>
        {!! Form::label("{$lang}[title]", trans('news::posts.form.title')) !!}
        {!! Form::text("{$lang}[title]", old("$lang.title", $oldTitle), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('news::posts.form.title')]) !!}
        {!! $errors->first("$lang.title", '<span class="help-block">:message</span>') !!}
    </div>
    <?php $old = isset($post->translate($lang)->content) ? $post->translate($lang)->content : ''; ?>
    @editor('content', trans('news::posts.form.content'), old("$lang.content", $old), $lang)

    <?php if (config('asgard.news.config.post.partials.translatable.edit') !== []): ?>
        <?php foreach (config('asgard.news.config.post.partials.translatable.edit') as $partial): ?>
        @include($partial)
        <?php endforeach; ?>
    <?php endif; ?>
</div>