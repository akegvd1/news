@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('news::posts.title.edit post') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.news.post.index') }}">{{ trans('news::posts.title.posts') }}</a></li>
        <li class="active">{{ trans('news::posts.title.edit post') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.news.post.update', $post->id], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-10">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers', ['fields' => ['title', 'slug']])
                <div class="tab-content">
                    <?php $i = 0; ?>
                    <?php foreach (LaravelLocalization::getSupportedLocales() as $locale => $language): ?>
                        <?php $i++; ?>
                        <div class="tab-pane {{ App::getLocale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('news::admin.posts.partials.edit-fields', ['lang' => $locale])
                        </div>
                    <?php endforeach; ?>
                    <?php if (config('asgard.news.config.post.partials.normal.edit') !== []): ?>
                        <?php foreach (config('asgard.news.config.post.partials.normal.edit') as $partial): ?>
                            @include($partial)
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.news.post.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
        <div class="col-md-2">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        <?php $oldSlug = isset($post->slug) ? $post->slug : ''; ?>
                        {!! Form::label("slug", "Slug") !!}
                        {!! Form::text("slug", old("slug", $oldSlug), ['class' => 'form-control slug', 'data-slug' => 'target', 'placeholder' => "Slug"]) !!}
                        {!! $errors->first("slug", '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label("status", 'Post status:') !!}
                        <select name="status" id="status" class="form-control">
                            <?php foreach ($statuses as $id => $status): ?>
                            <option value="{{ $id }}" {{ old('status', $post->status) == $id ? 'selected' : '' }}>
                                {{ $status }}
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div id="box-category" class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Categories</h3>
                </div>
                <div class="box-body">
                    <?php if (!$categories->isEmpty()) { ?>
                        <?php foreach ($categories as $cat) { ?>
                            <?php
                                $postCategories = $post->categories;
                                $selectedCat = [];
                                if (!$postCategories->isEmpty()) {
                                    $selectedCat = $postCategories->pluck('id')->toArray();
                                }
                                $oldCatInput = old('categories', $selectedCat);
                                $checkedAttr = (in_array($cat->id, $oldCatInput)) ? ' checked="checked"' : '' ;
                            ?>
                            <div class="checkbox" style="margin-left:{{ $cat->depth*15 }}px;">
                                <label for="cat-{{ $cat->id }}">
                                    <input name="categories[]" type="checkbox" value="{{ $cat->id }}" id="cat-{{ $cat->id }}" class="flat-blue" data-parentid="{{ (int) $cat->parent_id }}"<?php echo $checkedAttr ?>> {{ $cat->title }}
                                </label>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Tags</h3>
                </div>
                <div class="box-body">
                    @tags('asgardcms/news', $post)
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Tags</h3>
                </div>
                <div class="box-body">
                    @mediaSingle('thumbnail', $post)
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    {{-- <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp; --}}
@stop
@section('shortcuts')
    {{-- <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl> --}}
@stop

@push('js-stack')
    {{-- <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "" }
                ]
            });
        });
    </script> --}}
    <script>
        $( document ).ready(function() {
            $('#box-category input[type="checkbox"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            }).on('ifChecked', function(e){
                var parentId = $(this).data('parentid');
                $('#box-category #cat-' + parentId).iCheck('check');
            }).on('ifUnchecked', function(e){
                var currentId = $(this).val();
                $('#box-category input[data-parentid="' + currentId + '"]').iCheck('uncheck');
            });
        });
    </script>
@endpush
