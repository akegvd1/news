@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('news::posts.title.posts') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('news::posts.title.posts') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.news.post.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('news::posts.button.create post') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th  width="40">Id</th>
                                    <th>{{ trans('news::posts.table.title') }}</th>
                                    <th width="150">{{ trans('news::posts.table.slug') }}</th>
                                    <th width="40">{{ trans('news::posts.table.status') }}</th>
                                    <th width="120">{{ trans('core::core.table.created at') }}</th>
                                    <th width="100" data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($posts)): ?>
                                <?php foreach ($posts as $post): ?>
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.news.post.edit', [$post->id]) }}">
                                                {{ $post->id }}
                                            </a>
                                        </td>
                                        
                                        <td>
                                            <a href="{{ route('admin.news.post.edit', [$post->id]) }}">
                                                {{ $post->title }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.news.post.edit', [$post->id]) }}">
                                                {{ $post->slug }}
                                            </a>
                                        </td>
                                        <td>
                                            <span class="label {{ $post->present()->statusLabelClass }}">
                                                {{ $post->present()->status }}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.news.post.edit', [$post->id]) }}">
                                                {{ $post->created_at }}
                                            </a>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('admin.news.post.edit', [$post->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                                <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.news.post.destroy', [$post->id]) }}"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Id</th>
                                <th>{{ trans('news::posts.table.title') }}</th>
                                <th>{{ trans('news::posts.table.slug') }}</th>
                                <th>{{ trans('news::posts.table.status') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('news::posts.title.create post') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.news.post.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
