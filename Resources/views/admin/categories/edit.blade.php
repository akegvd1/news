@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('news::categories.title.edit category') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.news.post.index') }}">{{ trans('news::news.title.news') }}</a></li>
        <li><a href="{{ route('admin.news.category.index') }}">{{ trans('news::categories.title.categories') }}</a></li>
        <li class="active">{{ trans('news::categories.title.edit category') }}</li>
    </ol>
@stop

@section('content')
{{-- {{dd($data)}} --}}
    {!! Form::open(['route' => ['admin.news.category.update', $category->id], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">

                    <div class="form-group">
                        {!! Form::label("parent_id", trans('news::categories.form.parent')) !!}
                        <?php /*
                        {!! Form::select('parent_id', $categorySelectOptions, old('parent_id', $category->parent_id), ['class' => 'form-control']) !!}
                        */ ?>
                        <select class="form-control" name="parent_id">
                            <?php foreach ($categorySelectOptions as $key => $val) { ?>
                            <?php
                                $descendantsArr = $category->descendants()->get()->pluck('id')->toArray();
                                $selected       = ($key == old('parent_id', $category->parent_id)) ? 'selected="selected"' : '' ;
                                $disabled       = ($key == $category->id || in_array($key, $descendantsArr)) ? 'disabled="disabled"' : '' ;
                            ?>
                            <option value="{{ $key }}" {{ $selected }} {{ $disabled }}>{{ $val }}</option>
                            <?php } ?>
                        </select>
                    </div>

                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('news::admin.categories.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    {!! Form::normalInput('slug', trans('news::categories.form.slug'), $errors, old("slug", $category), ['autocomplete' => 'off']) !!}

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.news.category.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.news.category.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
