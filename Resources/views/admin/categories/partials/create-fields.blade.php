<div class="box-body">

    <?php
        if ($i == 1) {
            $attrOptions = [
                'data-slug' => 'source',
                'class' => 'form-control',
                'autocomplete' => 'off',
                'placeholder' => trans('news::categories.form.title')
            ];
        } else {
            $attrOptions = [
                'class' => 'form-control',
                'autocomplete' => 'off',
                'placeholder' => trans('news::categories.form.title')
            ];
        }
    ?>

    <div class='form-group{{ $errors->has("{$lang}.title") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[title]", trans('news::categories.form.title')) !!}
        {!! Form::text("{$lang}[title]", old("{$lang}[title]"), $attrOptions) !!}
        {!! $errors->first("{$lang}.title", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.detail") ? ' has-error' : '' }}'>
       {!! Form::label("{$lang}[detail]", trans('news::categories.form.detail')) !!}
       {!! Form::textarea("{$lang}[detail]", old("{$lang}[detail]"), ['class' => 'form-control', 'placeholder' => trans('news::categories.form.detail')]) !!}
       {!! $errors->first("{$lang}.detail", '<span class="help-block">:message</span>') !!}
   </div>
</div>
