<?php

return [
    'list resource' => 'List categories',
    'create resource' => 'Create categories',
    'edit resource' => 'Edit categories',
    'destroy resource' => 'Destroy categories',
    'title' => [
        'categories' => 'Category',
        'create category' => 'Create a category',
        'edit category' => 'Edit a category',
    ],
    'button' => [
        'create category' => 'Create a category',
    ],
    'table' => [
    ],
    'form' => [
        'slug' => 'Slug',
        'title' => 'Category Title',
        'detail' => 'Detail',
        'parent' => 'Select Parent Category'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
