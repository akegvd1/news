<?php

return [
    'list resource' => 'List posts',
    'create resource' => 'Create posts',
    'edit resource' => 'Edit posts',
    'destroy resource' => 'Destroy posts',
    'title' => [
        'posts' => 'Post',
        'create post' => 'Create a post',
        'edit post' => 'Edit a post',
    ],
    'button' => [
        'create post' => 'Create a post',
    ],
    'table' => [
        'title' => 'Title',
        'slug' => 'Slug',
        'status' => 'Status',
    ],
    'form' => [
        'title' => 'Title',
        'slug' => 'Slug',
        'content' => 'Content',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
