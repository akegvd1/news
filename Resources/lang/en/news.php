<?php

return [
    'list resource' => 'List news',
    'title' => [
        'news' => 'News',
        'create news' => 'Create a news',
        'edit news' => 'Edit a news',
    ],
];
