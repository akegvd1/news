<?php

return [
    'news.posts' => [
        'index' => 'news::posts.list resource',
        'create' => 'news::posts.create resource',
        'edit' => 'news::posts.edit resource',
        'destroy' => 'news::posts.destroy resource',
    ],
    'news.categories' => [
        'index' => 'news::categories.list resource',
        'create' => 'news::categories.create resource',
        'edit' => 'news::categories.edit resource',
        'destroy' => 'news::categories.destroy resource',
    ],
// append


];
