<?php

namespace Modules\News\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\News\Entities\Post;
use Modules\News\Entities\Category;
use Modules\News\Events\Handlers\RegisterNewsSidebar;
use Modules\News\Repositories\Cache\CacheCategoryDecorator;
use Modules\News\Repositories\Cache\CachePostDecorator;
use Modules\News\Repositories\CategoryRepository;
use Modules\News\Repositories\Eloquent\EloquentPostRepository;
use Modules\News\Repositories\Eloquent\EloquentCategoryRepository;
use Modules\News\Repositories\PostRepository;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Media\Image\ThumbnailManager;
use Modules\Tag\Repositories\TagManager;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;

class NewsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterNewsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('posts', array_dot(trans('news::posts')));
            $event->load('categories', array_dot(trans('news::categories')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('news', 'permissions');
        $this->publishConfig('news', 'config');
        
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->registerThumbnails();
        $this->app[TagManager::class]->registerNamespace(new Post());

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(PostRepository::class, function () {
                $repository = new EloquentPostRepository(new Post());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new CachePostDecorator($repository);
            }
        );

        $this->app->bind(CategoryRepository::class, function () {
                $repository = new EloquentCategoryRepository(new Category());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new CacheCategoryDecorator($repository);
            }
        );
    }

    private function registerThumbnails()
    {
        $this->app[ThumbnailManager::class]->registerThumbnail('newsThumb', [
            'fit' => [
                'width' => '150',
                'height' => '150',
                'callback' => function ($constraint) {
                    $constraint->upsize();
                },
            ],
        ]);
    }
}
